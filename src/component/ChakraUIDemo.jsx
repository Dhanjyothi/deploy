import React, { useState } from 'react'
import {ChakraProvider,Container,Heading,Text,Input,Button,Table,Thead,Tbody,Th,Tr,Td} from'@chakra-ui/react'
function ChakraUIDemo() {
    let [formData,setFormData]=useState({
        ename:'',
        email:''
    })
    let [formDataList,setFormDataList]=useState([])
    let handleChange=(e)=>{
        setFormData({...formData,[e.target.name]:e.target.value})
    }
    let handleSubmit=(e)=>{
        e.preventDefault()
        //console.log(formData)
        setFormDataList([...formDataList,formData])
    }
  return (
    // <ChakraProvider>
      <Container>
        <Heading as="h1">
            Welcome to ChakraUI Demo
        </Heading>
        <form onSubmit={handleSubmit}>
            <Input type='text' name='ename' placeholder='Enter your name' onChange={handleChange}/>
            <Input type='text' name='email' placeholder='Enter your email' onChange={handleChange}/>
       <Button type='submit' colorScheme='teal'>Submit</Button>
        </form>
        {formDataList.length>0 &&(
            <Table variant="striped" colorScheme='teal' >
<Thead>
    <Tr>
        <Th>Name</Th>
        <Th>Email</Th>
    </Tr>
</Thead>
<Tbody>
    {formDataList.map(data=>(
        <Tr>
            <Td>{data.ename}</Td>
            <Td>{data.email}</Td>
        </Tr>
    ))}
</Tbody>
            </Table>
        )}
      </Container>
    // </ChakraProvider>
  )
}

export default ChakraUIDemo
