import React, { Component } from 'react'

 class MyFirstCom extends Component {
        constructor(props) {
      super(props)
    
      this.state = {
         count:1
      }
      console.log('constructor')
    // this.increment= this.increment.bind(this)//external binding
    }
    increment=()=>//internal binding
    {
        //console.log(this)
        this.setState({count:this.state.count+1})
    }
    componentDidMount(){
console.log('component did mount')
    }
    componentDidUpdate()
    {
        console.log('component did update')
    }
  render() {
    console.log('render')
    return (
      <div>
        <h1>{this.state.count}</h1>
        <button onClick={this.increment}>Increment</button>
      </div>
    )
  }
}

 export default MyFirstCom
