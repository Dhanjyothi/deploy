import React, { useState } from 'react'

function MyFirstFun() {
    const [count,setCount]=useState(0)
    // let increment=()=>{
    //     setCount(count+1)
    // }
  return (
    <div>
      {count}
      <button onClick={()=>setCount(count+1)}>Increment</button>
    </div>
  )
}

export default MyFirstFun
