import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

export const MyForm = () => {
  const schema = yup.object().shape({
    name: yup.string().required('Name is required'),
    email: yup.string().email('Invalid email format').required('Email is required'),
    password: yup.string()
    .required('Password is required')
    .test('password', 'Password must contain at least 1 lowercase, 1 uppercase, and 1 digit', (value) => {
          return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/.test(value);
    })
  })
  
  const { register, formState: { errors, isValid }, trigger ,handleSubmit} = useForm({
    resolver: yupResolver(schema),
  });
  let onSubmit = (data) => {
    console.log('entered in to onSubmit', data);
};
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>
        Username:  
        <input
          {...register('name')}
          onBlur={() => trigger('name')} // Trigger validation on blur
        />
        {errors.name && <p className="error-message">{errors.name.message}</p>}
      </div>
      
      <div>
        Email: 
        <input
          {...register('email')}
          onBlur={() => trigger('email')} // Trigger validation on blur
        />
        {errors.email && <p className="error-message">{errors.email.message}</p>}
      </div>

      <div>
        Password: 
        <input
          {...register('password')}
          onBlur={() => trigger('password')} // Trigger validation on blur
        />
        {errors.password && <p className="error-message">{errors.password.message}</p>}
      </div>

      <button type="submit" disabled={!isValid}>Submit</button>
    </form>
  );
};
