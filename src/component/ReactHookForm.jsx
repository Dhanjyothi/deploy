import React from 'react';
import { useForm } from 'react-hook-form';

function ReactHookForm() {
    let { register, handleSubmit, formState: { errors } } = useForm();

    let onSubmit = (data) => {
        console.log('entered in to onSubmit', data);
    };
   
    return (
        <form onSubmit={handleSubmit(onSubmit)} >
            <div>
                <label>Username</label>
                <input type='text' id='username' {...register('username', { required: true, minLength: 3 })} />
                {errors.username && errors.username.type === "required" && <span>Username is required</span>}
                {errors.username && errors.username.type === "minLength" && <span>Username should contain at least 3 characters</span>}
            </div>

            <div>
                <label>Password</label>
                <input type='text' id='password' {...register('password', { required: true })} />
                {errors.password  && <span style={{ color: 'red' }}>Password is required</span>}
            </div>

            <button type='submit'>Submit</button>
        </form>
    );
}

export default ReactHookForm;
