import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

const schema = yup.object().shape({
    name: yup.string().required('Name is required'),
    email: yup.string().email('Invalid email').required('Email is required'),
    mobile: yup.string().required('Mobile is required'),
    

});

function ReactHookYup() {
    const { register, handleSubmit, formState: { errors, isValid } } = useForm({
        resolver: yupResolver(schema),
    });

    const onSubmit = (data) => {
        console.log('Form data:', data);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div>
                <label>Name</label>
                <input
                    type='text'
                    {...register('name')}
                />
                {errors.name && <span>{errors.name.message}</span>}
            </div>
            <div>
                <label>Email</label>
                <input
                    type='text'
                    {...register('email')}
                />
                {errors.email && <span>{errors.email.message}</span>}
            </div>
            <div>
                <label>Mobile</label>
                <input
                    type='text'
                    {...register('mobile')}
                />
                {errors.mobile && <span>{errors.mobile.message}</span>}
            </div>
            <button type='submit'>Submit</button>
        </form>
    );
}

export default ReactHookYup;
