import React from 'react';
import { useForm } from 'react-hook-form';

export const Test = () => {
  const { register, handleSubmit, formState: { errors, isValid } } = useForm({
    mode: 'onBlur' // Trigger validation on blur
  });

  const onSubmit = (data) => {
    console.log(data);
    // Handle form submission
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>
        Username:  
        <input
          {...register('name', { required: 'Name is required' })}
        />
        {errors.name && <p className="error-message">{errors.name.message}</p>}
      </div>
      
      <div>
        Email: 
        <input
          {...register('email', { required: 'Email is required'})}
        />
        {errors.email && <p className="error-message">{errors.email.message}</p>}
      </div>

      <div>
        Enter Password: 
        <input
          {...register('password', { 
            required: 'Password is required',
            minLength: { value: 8, message: 'Password must be at least 8 characters long' }
          })}
        />
        {errors.password && <p className="error-message">{errors.password.message}</p>}
      </div>

      <button type="submit" disabled={!isValid}>Submit</button>
    </form>
  );
};
