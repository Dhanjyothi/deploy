import {Container,Table,Thead,Tbody,Th,Tr,Td} from'@chakra-ui/react'
import React, { useEffect,useState } from 'react'
import axios from 'axios'
function GetAllUsers() {
    let [formDataList,setFormDataList]=useState([])
    useEffect(()=>{
axios.get('http://localhost:4000/api/getAll')
.then(res=>setFormDataList(res.data))
.catch(err=>console.log(err))
    },[])
  return (
    <Container>
    {formDataList.length>0 &&(
        <Table variant="striped" colorScheme='teal' >
<Thead>
<Tr>
    <Th>Username</Th>
    <Th>Password</Th>
</Tr>
</Thead>
<Tbody>
{formDataList.map(data=>(
    <Tr>
        <Td>{data.username}</Td>
        <Td>{data.password}</Td>
    </Tr>
))}
</Tbody>
        </Table>
    )}
  </Container>

)
}

export default GetAllUsers
