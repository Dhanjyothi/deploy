import React from 'react'
import {useForm} from 'react-hook-form'
import axios from 'axios'
import {FormLabel,FormControl,FormErrorMessage,Input,Button} from '@chakra-ui/react'
function Register() {
    let {handleSubmit,register,formState:{errors}}=useForm()
    let onSubmit=async(values)=>{
await axios.post('http://localhost:4000/api/save',values)
.then(res=>console.log(res.data))
.catch(err=>console.log(err))
    }
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
        <FormControl>
      <FormLabel >Username</FormLabel>
      <Input  {...register('username',{required:'username is required',minLength:{value:4,message:'min length should be 4'}})}/>
   <FormErrorMessage>
    {errors.username && errors.username.message}
    </FormErrorMessage>
    </FormControl>
    <FormControl>
      <FormLabel >Password</FormLabel>
      <Input  {...register('password',{required:'passowrd is required'})}/>
   <FormErrorMessage>
    {errors.username && errors.password.message}
    </FormErrorMessage>
    </FormControl>
    <Button colorScheme='teal'type='submit'>Register</Button>

    </form>
  )
}

export default Register

