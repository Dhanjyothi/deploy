import React, { useEffect ,useState} from 'react'
import axios from 'axios'
function UseEffectDemo() {
    let [posts,setPosts]=useState([])
    let [id,setId]=useState(1)
    useEffect(()=>{
        axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
        .then(res=>{
          
            setPosts(res.data)})
        .catch(err=>console.log(err))
    },[id])
  return (
    <div>
       Id: <input type='text' id="id" value={id} onChange={(e)=>setId(e.target.value)}/>
      <li> {posts.title}</li>
      {/* {
        posts.map(p=>(
            <li>{p.title}</li>
        ))
      } */}
    </div>
  )
}

export default UseEffectDemo
