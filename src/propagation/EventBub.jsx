import React,{Component} from 'react'

class ParentComponent extends Component
{
    handleParentClick=()=>{
        console.log('Parent clicked')
    }
    render()
    {
        return(
            <div onClick={this.handleParentClick} style={{border:'1px solid black'}}>
                <ChildComponent/>
            </div>
        )
    }
}
class ChildComponent extends Component{
    handleChildClick=(event)=>{
        event.stopPropagation();
        console.log('Child clicked')
    }
    render()
    {
        return(
            <button onClick={this.handleChildClick}>Click Me</button>
             
           
        )
    }
}

export default ParentComponent